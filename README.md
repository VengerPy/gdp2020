# GdP2020

Lösungen zu den Übungsaufgaben

# Nützliche Links
Python Cheat-Sheets:
https://ehmatthes.github.io/pcc/cheatsheets/README.html

Lernmaterial/Slides zu Python:
https://marko-knoebl.github.io/slides/#python

Praxisorientiertes Python-Buch:
https://automatetheboringstuff.com/

Offizielle Python-Dokumentation:
https://docs.python.org/3/

CodeCamp - Komplett Kurs für Anfänger (Video):
https://www.youtube.com/watch?v=rfscVS0vtbw

RealPython - Introduction to Python:
https://realpython.com/learning-paths/python3-introduction/