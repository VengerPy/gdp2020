#  Binäre Suche
#  Aufgabe 1
#  Übung 1.1

schritte_seq = 1024
schritte_bin = 10 # log von 1024 zur Basis 2

#  Übung 1.2

schritte_seq = 4*1024
schritte_bin = 12 # log von 4096 zur Basis 2

#  Aufgabe 2

def binary_search(liste, search):
    return binary_search_recursive(liste, search, 0, len(liste)-1)
    
def binary_search_recursive(liste, search, left, right):
    if left > right:
        return None
    else: 
        mid = (left + right) // 2
        element = liste[mid]
        
        if search == element:
            return mid
        elif search > element:
            return binary_search_recursive(liste, search, mid + 1, right)
        else:
            return binary_search_recursive(liste, search, left, mid - 1)

#  Caesar Verschlüsselung

def caesar(text, rot):
    return "".join([chr(((ord(l)-65+rot%26) % 26) + 65) for l in text])

#  Insertionsort

def insertionsort(liste):
    for i in range(len(liste)):
        wert = liste[i]
        j = i
        while (j > 0) and (liste[j-1]) > wert:
            liste[j] = liste[j-1]
            j = j - 1
        liste[j] = wert
    return liste

# Selectionsort

def selectionsort(liste):
    end = len(liste)
    start = 0
    
    while start < end:
        index_of_minimum = start
        
        for index, elem in enumerate(liste[start:], start):
            if elem < liste[index_of_minimum]:
                index_of_minimum = index
                
        liste[start], liste[index_of_minimum] = liste[index_of_minimum], liste[start]
        start +=1
        
    return liste
