def bubblesort(liste):
    remaining = len(liste)
    
    while remaining > 1:
        change = False
        
        for i in range(1, remaining):
            num1 = liste[i-1]
            num2 = liste[i]
            if num1 <= num2:
                liste[i-1], liste[i] = num2, num1
                change = True
        
        if not change:
            break 
        remaining -= 1
        
    return liste