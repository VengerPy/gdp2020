#  Liste Statistik

def statistik(liste):
    return min(liste), max(liste), sum(liste)/len(liste)

#  Rekursion
#  Übung 1.1
def sum_recursive(liste):
    if len(liste) == 0:
        return 0
    else:
        return liste.pop() + sum_recursive(liste)

#  Übung 1.2
def count_recursive(liste):
    try:
        liste.pop()
        return 1 + count_recursive(liste)
    except:
        return 0

#  Übung 1.3
def swap_list_order(liste):
    length = len(liste)
    
    if length > 1:
        return [liste.pop()] + swap_list_order(liste)
    elif length == 1:
        return [liste.pop()]
    else:
        return []

#  Übung 1.4
def find_element(liste, find, index=0):
    if index == len(liste):
        return False
    
    if isinstance(liste[index], list):
        branch = find_element(liste[index], find)
        if not isinstance(branch, int):
            return [index] + branch
    elif liste[index] == find:
        return [index]

    return find_element(liste, find, index+1)

#  Übung 1.5
def do_something_recursive(liste):
    if len(liste) < 1:
        return []
    else:
        elem = do_more_things_recursive(liste.pop(0))
        return [elem] + do_something_recursive(liste)
    
def do_more_things_recursive(num):
    if num < 1:
        return []
    else:
        return [1] + do_more_things_recursive(num-1)

#  Summe Zahlen
def sum_natural(limit):
    nums = list(range(limit+1))
    pos, summe = 0, 0
    
    while pos < len(nums):
        summe += nums[pos]
        pos += 1
        
    return summe

#Übersetzer
def translate(word):
    d = {
        "Zug" : "train",
        "Hochschule" : "university",
        "Küche" : "kitchen",
        "Computer" : "computer"
    }
    
    return d.get(word)
