# Fahrenheit

def fahrenheit_to_celsius(fahrenheit):
    return 5/9*(fahrenheit-32)
    
def celsius_to_kelvin(celsius):
    return celsius+273.15


#  Bogenmaß

from math import pi

def rad_to_deg(rad):
    return rad*180/pi
