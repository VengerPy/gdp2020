#  Steuern

def steuer(einkommen, vermoegen):
    se = calc_se(einkommen)
    sv = calc_sv(vermoegen)
    return se * einkommen + sv * vermoegen
    
def calc_se(einkommen):
    se = None
    if einkommen < 20000:
        se = 0.05
    elif einkommen >= 20000 and einkommen < 40000:
        se = 0.15
    elif einkommen >= 40000 and einkommen < 100000:
        se = 0.25
    else:
        se = 0.4
    return se

def calc_sv(vermoegen):
    sv = None
    if vermoegen < 10000:
        sv = 0.01
    elif vermoegen >= 10000 and vermoegen < 50000:
        sv = 0.03
    elif vermoegen >= 50000 and vermoegen < 200000:
        sv = 0.05
    else:
        sv = 0.1
    return sv

#  Steuern Alternative mit range() und dict {} 

def steuer(einkommen, vermoegen):
    percent_se, percent_sv = 0.4, 0.1
    se = {range(20000): 0.05, range(20000, 40000): 0.15,
          range(40000, 100000): 0.25}
    sv = {range(10000): 0.01, range(10000, 50000): 0.03,
          range(50000, 200000): 0.05}
    for sek, svk in zip(se, sv):
        if einkommen in sek: percent_se = se.get(sek)
        if vermoegen in svk: percent_sv = sv.get(svk)
    return percent_se * einkommen + percent_sv * vermoegen

#  Zahlenvergleich

def zahlenvergleich(x, y):
    return (max(x, y), min(x, y))