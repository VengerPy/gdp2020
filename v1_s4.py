#  Geometrische Reihe

def geometrische_reihe():
    summe = 0.0
    last = None
    power = -1
    
    while summe != last:
        last = summe
        summe += 2**power
        power -= 1
    
    return summe

#  Geometrische Reihe (Alternative mit Itertools)

from itertools import accumulate, count

def geometrische_reihe():
    last = None
    for i in accumulate((2**i for i in count(-1, -1))):
        if i == last: return i
        last = i

#  Tannenbaum

def tannenbaum(levels, level_width):
    level_step = 2
    trunk_height = 3
    trunk_width = 3
    text = list()
    
    for _ in range(trunk_height):
        spaces = (level_width - trunk_width)//2
        spaces = " "*spaces
        hashtags = "#"*trunk_width
        text.append(spaces + hashtags)
    
    for _ in range(levels):
        hashtags = level_width // 2
        
        while hashtags >= 0:
            spaces = " " * (level_width // 2 - hashtags)
            tags = "#" * hashtags
            text.append(spaces + tags + "#" + tags)
            hashtags -= level_step
    
    text.reverse()
    return "\n".join(text) + "\n"
