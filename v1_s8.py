from random import randint

def quicksort(array):
    if len(array) <= 1:
        return array # Liste der Länge 1 oder kürzer ist sortiert
    else:
        _quick_recursion(array, 0, len(array)-1)
        return array
    
def _quick_recursion(array, left, right):
    if left >= right:
        return # Rekursionsende
    
    #Läufer links, Läufer rechts
    start, end = left, right
    
    #Pivotelement auswählen
    pivot_pos = randint(left, right)
    pivot = array[pivot_pos]

    while start < end:    
        # Linke Seite nach Werten >= Pivot durchsuchen
        while start < end and array[start] < pivot:
            start += 1
            
        # Rechte Seite nach Werten < Pivot durchsuchen
        while end > start and array[end] >= pivot:
            end -= 1 
        
        #Tausche
        array[start], array[end] = array[end], array[start]
        
        #Tracking der Pivot-Position
        if start == pivot_pos:
            pivot_pos = end
    
    #Das Pivot-Element wird auf den Treffpunkt des rechten/linken Läufers getauscht
    if array[start] > pivot:
        array[start], array[pivot_pos] = array[pivot_pos], array[start]

    #Rekursionsaufrufe der Teillisten
    _quick_recursion(array, left, start-1)
    _quick_recursion(array, start+1, right)
    
    return
