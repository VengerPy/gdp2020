#  Hashtabelle
#  Übung 1.1

def calc_hash(key):
    hashValue = 0
    for i, letter in enumerate(key):
        hashValue += ord(letter)*(i+1)
    return hashValue

#  Übung 1.2

def get(self, key):
    hashValue = self.calc_hash(key) % self.number_slots
    dq = self.hashtable[hashValue]
    for nTuple in dq:
        if nTuple.key == key:
            return nTuple.value
    raise KeyError("Given key is not in hashtable")

#  Übung 1.3

def _remove_table_key_item(self, key, table_key):
    dq = self.hashtable[table_key]
    
    for i, nTuple in enumerate(dq):
        if nTuple.key == key:
            dq.remove(dq[i])
            return
        
    raise KeyError("Given key is not in hashtable")

def set(self, key, value):
    hashValue = self.calc_hash(key) % self.number_slots
    try:
        self._remove_table_key_item(key, hashValue)
    except:
        pass
    self.hashtable[hashValue].append(Element(key, value))

#  Übung 1.4

def remove(self, key):
    hashValue = self.calc_hash(key) % self.number_slots
    self._remove_table_key_item(key, hashValue)

#  Übung 1.5

def translate(word):
    hashtable = StringHashtable(4)
    hashtable["Zug"] = "train"
    hashtable["Hochschule"] = "university"
    hashtable["Küche"] = "kitchen"
    hashtable["Computer"] = "computer"

    try:
        return hashtable.get(word)
    except:
        return None

#  Mergesort

def merge(left, right):
    sortedList = []
    indexL, indexR = 0, 0
    lenLeft, lenRight = len(left), len(right)
    while indexL < lenLeft and indexR < lenRight:
        if left[indexL] < right[indexR]:
            sortedList.append(left[indexL])
            indexL += 1
        else:
            sortedList.append(right[indexR])
            indexR += 1
    sortedList += left[indexL:] + right[indexR:]
    return sortedList

def mergesort(liste):
    length = len(liste)
    
    if length <= 1:
        return liste
    else:
        left = mergesort(liste[:length//2])
        right = mergesort(liste[length//2:])
        return merge(left, right)
